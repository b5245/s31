// steps: index.js - models folder then task.js - controllers folder then taskController.js - routes folder then taskRoute.js 

// Setup the dependencies
const express = require("express");
const mongoose = require("mongoose");
const taskRoute = require("./routes/taskRoute");

// Setup Server
const app = express();
const port = 3001;

app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Database Connection
mongoose.connect("mongodb+srv://admin:admin@wdc028-course-booking.mgfy3.mongodb.net/b177-to-do?retryWrites=true&w=majority", { 
    useNewUrlParser: true, 
    useUnifiedTopology: true
});

app.use(`/tasks`, taskRoute)

// Server  Listening
app.listen(port, () => console.log(`Now listening to port ${port}`));