const express = require("express");
const res = require("express/lib/response");

// Creates a router that functions as a routing system
const router = express.Router()

// Import the taskControllers
const taskController = require("../controllers/taskControllers");

// Routes to get all the task
// endpoint: localhost:3001/tasks
router.get("/", (req, res) =>{
    taskController.getAllTasks().then(resultFromController => res.send(resultFromController));
});

// Route to Create a Task
router.post("/", (req, res) => {
    taskController.createTask(req.body).then(resultFromController => res.send(resultFromController));
})

// Route to delete task
router.delete(`/:id`, (req,res) => {
    taskController.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController));
})

// Route to Update Task
router.put(`/:id`, (req,res) => {
    taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
})

// Rout for Specific Task
router.get(`/:id`, (req,res) => {
    taskController.specificTask(req.params.id).then(resultFromController => res.send(resultFromController));
})

// Route for update Status
router.put(`/:id/complete`, (req,res) => {
    taskController.updateStatus(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
})

// export the router object to be used in index.js
module.exports = router;

