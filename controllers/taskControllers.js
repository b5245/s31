const req = require("express/lib/request");
const { update } = require("../models/task");
const Task = require("../models/task")

// Controller function for getting all the tasks
module.exports.getAllTasks = () => {
    return Task.find({}).then(result =>{
        return result;
    })
}

// Controller function for creating a task
module.exports.createTask = (requestBody) => {
    // Create a task object based on the mongoose model "Task"
    let newTask = new Task({
        // Sets  the "name" property with the value received from the client
        name : requestBody.name
    })

    return newTask.save().then((task, error) => {
        if(error){
            console.log(error);
            return false;
        }
        // Save is successful
        else{
            return task;
        }
    })
}

// Controller to delete Task
module.exports.deleteTask = (taskId) => {
    return Task.findByIdAndRemove(taskId).then((removedTask, err) => {
        if(err){
            console.log(err);
            return false;
        }
        else{
            return removedTask
        }
    })
}

// Controller to update Task
module.exports.updateTask = (taskId, newContent) => {
    return Task.findById(taskId).then((result, error) => {
        if(error){
            console.log(error)
            return false;
        }

        result.name = newContent.name;

        // Save the updated result in the mongoDb database
        return result.save().then((updatedTask, saveErr) => {
            if(saveErr){
                console.log(saveErr);
                return false;
            }
            else{
                return updatedTask
            }
        })
  
    })
}

// Controller for Specific Task
module.exports.specificTask = (taskId) => {
    return Task.findById(taskId).then((result, error) => {
        if(error){
            console.log(error);
            return false;
        }

        return result;

    })
}

module.exports.updateStatus = (taskId, newContent) => {
    return Task.findById(taskId).then((result, error) => {
        if(error){
            console.log(error);
            return false;
        }

        result.status = newContent.status;

        return result.save().then((updatedStatus, saveErr) => {
            if(saveErr){
                console.log(saveErr);
                return false;
            }
            else{
                return updatedStatus
            }
        })
    })
}